# Running the app



**app-name** can be anything i.e angula-app, it's a tag used to refernce the container, -t (tag). The period at the end
represents the current directory.

```bash
docker build -t app-name .
```


run the container on port 4200. 

```bash
docker run -p 4200:4200 app-name
```
