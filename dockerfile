
# import latest version of node into the container
FROM node:latest

# create a a new director in the container
RUN mkdir -p /app/src

#  set the working directory in the container
# docker will use this directory as the working directory
WORKDIR /app/src


# copy the package.json file into the container
# this will be used to install the dependencies in our container

COPY package.json /app/src

RUN npm install


# the first period indicates the current working directory, the secdon one indicates the directoy in the container and since the
# container working directory has been set to /app/src that's where everything will be copied, instead of the period
# you could also use /app/src
# this will copy everything in our project to the container
COPY . .

EXPOSE 4200

CMD ng serve --host 0.0.0.0